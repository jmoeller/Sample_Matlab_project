function [ nu ] = op( parall,T )
% Phenomenological fit of order parameter

% BEGIN ESSENTIAL HEADER
% Generate variables from parall cell array
% Needs to be in every fitting function
for i=1:length(parall{1})
    varbls=genvarname(parall{1}{i});
    eval([varbls '=  parall{2}(i);']);
end
% END ESSENTIAL HEADER

nu=zeros(length(T),1);
for i=1:length(T)    
    if (T(i) < Tc)
        nu(i)=nu0*(1-(T(i)/Tc)^alp)^be + nubg; % alpha is not allowed
    else
        nu(i)=0;
    end
end

end

