function [ F ] = FmuF_LRO_2(parall,t)
% FmuF signal with Lorentzian damping, Lorentzian background and
% non-decaying background

% BEGIN ESSENTIAL HEADER
% Generate variables from parall cell array
% Needs to be in every fitting function
for i=1:length(parall{1})
    varbls=genvarname(parall{1}{i});
    eval([varbls '=  parall{2}(i);']);
end
% END ESSENTIAL HEADER

phi=180/pi*phi; % convert to degrees
r1=[0 0 -mr1];
r2=mr2*[sin(phi) 0 cos(phi)];

rm=[0 0 0];
B=[Bx By Bz];

%[Ptot] = fmuf(B,r1,rm,r2,t,A_muF,A_FF) las two optional
Gfmuf=fmuf(B,r1,rm,r2,t);

F=A1*Gfmuf.*exp(-la1*t)+A2*exp(-la2*t)+Abg+A3*(1/3+2/3*exp(-(sig*t).^2).*cos(2*pi*nu*t));

end
