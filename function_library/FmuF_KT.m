function [ F ] = FmuF_KT(parall,t)
% FmuF signal with Lorentzian damping, KT background 

% Simplified version: no B field and no t0 offset (less parameters in fit
% table).

% BEGIN ESSENTIAL HEADER
% Generate variables from parall cell array
% Needs to be in every fitting function
for i=1:length(parall{1})
    varbls=genvarname(parall{1}{i});
    eval([varbls '=  parall{2}(i);']);
end
% END ESSENTIAL HEADER

r1=[0 0 -mr1];

r2=mr2*[sin(phi) 0 cos(phi)];
%r2=-r1;
rm=[0 0 0];
B=[0 0 0; 0 0 0; 0 0 0]; % no field at all three positions (F1 F2 mu)

Gfmuf=fmuf(B,r1,rm,r2,t);

F=Afmf*Gfmuf.*exp(-la1*t) + A2*exp(-la2*t) ... 
    + AKT*(1/3+2/3*exp(-1/2*delta^2*t.^2).*(1-delta^2*t.^2));

end