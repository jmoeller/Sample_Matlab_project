function [ F ] = FmuF_LRO_s(parall,t)
% FmuF signal with Lorentzian damping, Lorentzian background and
% non-decaying background
% Simplified version: no B field and no t0 offset (less parameters in fit
% table).

% BEGIN ESSENTIAL HEADER
% Generate variables from parall cell array
% Needs to be in every fitting function
for i=1:length(parall{1})
    varbls=genvarname(parall{1}{i});
    eval([varbls '=  parall{2}(i);']);
end
% END ESSENTIAL HEADER

phi=pi/180*phi; % convert to radians!! 
r1=[0 0 -mr1];
%r2=-r1;
r2=mr2*[sin(phi) 0 cos(phi)];

rm=[0 0 0];
%B=[1e-3*Bt1 0 1e-3*Bl1; 0.001*sin(phi)*Bl2 0.001*Bt2 0.001*cos(phi)*Bl2 ; 1e-3*Bxm 1e-3*Bym 1e-3*Bzm]; % no field at all three positions (F1 F2 mu)
B=[0 0 0 ; 0 0 0 ; 0 0 0];

%[Ptot] = fmuf(B,r1,rm,r2,t,A_muF,A_FF) las two optional
Gfmuf=fmuf(B,r1,rm,r2,t);

F=A1*Gfmuf.*exp(-la1*t)+A2*exp(-la2*t)+Abg+A3*(1/3+2/3*exp(-(sig*t).^2).*cos(2*pi*nu*t)) ... 
    + A4*(1/3+2/3*exp(-(sig2*t).^2).*cos(2*pi*nu2*t));

end
