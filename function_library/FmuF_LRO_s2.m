function [ F ] = FmuF_LRO_s2(parall,t)
% FmuF signal with Lorentzian damping, Lorentzian background and
% non-decaying background
% Simplified version: no B field and no t0 offset (less parameters in fit
% table).

% BEGIN ESSENTIAL HEADER
% Generate variables from parall cell array
% Needs to be in every fitting function
for i=1:length(parall{1})
    varbls=genvarname(parall{1}{i});
    eval([varbls '=  parall{2}(i);']);
end
% END ESSENTIAL HEADER

phi=pi/180*phi; % convert to radians!! 
r1=[0 0 -mr1];

%r2=-r1;
r2=mr2*[sin(phi) 0 cos(phi)];

rm=[0 0 0];
B=[0 0 0; 0 0 0; 0 0 0]; % no field at all three positions (F1 F2 mu)

Gfmuf=fmuf(B,r1,rm,r2,t);

% second fmuf system
m2r2=m2r1;
r21=[0 0 -m2r1];
r22=[0 0 m2r2];

Gfmuf2=fmuf(B,r21,rm,r22,t);

F=A1*(frac*Gfmuf+(1-frac)*Gfmuf2).*exp(-la1*t)+A2*exp(-la2*t)+Abg+A3*(1/3+2/3*exp(-(sig*t).^2).*cos(2*pi*nu*t)) ... 
    + A4*(1/3+2/3*exp(-(sig2*t).^2).*cos(2*pi*nu2*t));

end
