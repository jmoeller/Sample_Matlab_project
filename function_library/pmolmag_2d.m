function [ y ] = pmolmag_2d(parall,x)
% Two frequencies with Lorentzian damping and a Lorentzian

% BEGIN ESSENTIAL HEADER
% Generate variables from parall cell array
% Needs to be in every fitting function
for i=1:length(parall{1})
    varbls=genvarname(parall{1}{i});
    eval([varbls '=  parall{2}(i);']);
end
% END ESSENTIAL HEADER

phi1=phi1/180*pi;
phi2=phi2/180*pi;
phi3=phi3/180*pi;

A2=0.5*A1; 
A3=0.25*A1;

nu2=0.65*nu1; 
nu3=0.55*nu1; 

y=A1*exp(-la1*x).*cos(2*pi*x*nu1+phi1) ...
    +A2*exp(-la2*x).*cos(2*pi*x*nu2+phi2) ...
    + A3*exp(-la3*x).*cos(2*pi*x*nu3+phi3) + A4*exp(-la4*x) ...
    +  AKT*(1/3+2/3*exp(-1/2*delta^2*x.^2).*(1-delta^2*x.^2)); 
end