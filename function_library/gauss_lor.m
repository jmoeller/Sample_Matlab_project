function [ y ] = gauss_lor(parall,x)
% Phenomenological fit of order parameter
% assumed sign of T is +

% BEGIN ESSENTIAL HEADER
% Generate variables from parall cell array
% Needs to be in every fitting function
for i=1:length(parall{1})
    varbls=genvarname(parall{1}{i});
    eval([varbls '=  parall{2}(i);']);
end
% END ESSENTIAL HEADER

y=A*exp(-(x-xc).^2/(2*sig^2))+B*exp(-(x-xcl)/lambda) + yoff;
        
end
