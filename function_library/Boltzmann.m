function [ y ] = Boltzmann(parall,x)
% Boltzmann factor or exponential activation
% E (meV), T(K)

% BEGIN ESSENTIAL HEADER
% Generate variables from parall cell array
% Needs to be in every fitting function
for i=1:length(parall{1})
    varbls=genvarname(parall{1}{i});
    eval([varbls '=  parall{2}(i);']);
end
% END ESSENTIAL HEADER

kb = 1.381*10^(-23);          % kB in SI

EmeV=E*0.001*1.602*10^(-19);
kT=kb * x;
y=A.*exp(-EmeV./kT)+B;
        
end
