function [ F ] = FmuFonly(parall,t)
% FmuF signal with Lorentzian damping, Lorentzian background and
% non-decaying background

% BEGIN ESSENTIAL HEADER
% Generate variables from parall cell array
% Needs to be in every fitting function
for i=1:length(parall{1})
    v=genvarname(parall{1}{i});
    eval([v '=  parall{2}(i);']);
end
% END ESSENTIAL HEADER

% r1=[0 0 -rz];
% r2=[r2x 0 rz];
 r1=[0 0 r1z];
 r2=[r2x 0 r2z];


rm=[0 0 0];
B=[Bx By Bz];

for i=1:length(t)
    t(i)=t(i)-toff;
end

%[Ptot] = fmuf(B,r1,rm,r2,t,A_muF,A_FF) las two optional
Gfmuf=fmuf(B,r1,rm,r2,t);

F=A1*Gfmuf.*exp(-la1*t)+A2*exp(-la2*t)+Abg;

end
