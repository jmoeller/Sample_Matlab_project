function [ F ] = FmuF_LRO(parall,t)
% FmuF signal with Lorentzian damping, Lorentzian background and
% non-decaying background

% BEGIN ESSENTIAL HEADER
% Generate variables from parall cell array
% Needs to be in every fitting function
for i=1:length(parall{1})
    varbls=genvarname(parall{1}{i});
    eval([varbls '=  parall{2}(i);']);
end
% END ESSENTIAL HEADER

% r1=[0 0 -rz];
% r2=[r2x 0 rz];
 r1=[0 0 r1z];
 r2=[r2x 0 r2z];

rm=[0 0 0];
B=[Bx By Bz];

for i=1:length(t)
    t(i)=t(i)-t0;
end

%[Ptot] = fmuf(B,r1,rm,r2,t,A_muF,A_FF) las two optional
Gfmuf=fmuf(B,r1,rm,r2,t);

F=A1*Gfmuf.*exp(-la1*t)+A2*exp(-la2*t)+Abg+A3*(1/3+2/3*exp(-(sig*t).^2).*cos(2*pi*nu*t));

end
