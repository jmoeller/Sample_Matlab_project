function [ y ] = my_sqrt(parall,x)

% BEGIN ESSENTIAL HEADER
% Generate variables from parall cell array
% Needs to be in every fitting function
for i=1:length(parall{1})
    varbls=genvarname(parall{1}{i});
    eval([varbls '=  parall{2}(i);']);
end
% END ESSENTIAL HEADER

y=x*0;
for i=1:length(x)
    %x(i)
    %xrescale*x(i)
    if(xrescale*x(i)<=xc)
        y(i)=A0*sqrt(1-xrescale*x(i)/xc) + yoff;
    elseif(xrescale*x(i)>xc)
        y(i)=0;
    end
end

end