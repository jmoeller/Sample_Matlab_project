function [ y ] = freq_x_gauss_p_lor(parall,x)
% Two frequencies with Lorentzian damping and a Lorentzian

% BEGIN ESSENTIAL HEADER
% Generate variables from parall cell array
% Needs to be in every fitting function
for i=1:length(parall{1})
    varbls=genvarname(parall{1}{i});
    eval([varbls '=  parall{2}(i);']);
end
% END ESSENTIAL HEADER

phi1=phi1/180*pi;

%A1=0.864*A2; 
%nu1=0.378*nu2; 

y=A1*exp(-la1*x).*cos(x*nu1+phi1) ...
    + A3*exp(-la3*x) ...
    + Ab;
end