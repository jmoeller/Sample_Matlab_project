function [ nr1 dnr1 nr2 dnr2 theta dtheta ] = get_rang(par,errs)
% Calculates angle and r separation and errors on them

% BEGIN ESSENTIAL HEADER
% Generate variables from parall cell array
% Needs to be in every fitting function
for i=1:length(parall{1})
    v=genvarname(parall{1}{i});
    eval([v '=  parall{2}(i);']);
end
% END ESSENTIAL HEADER


r1=[0 0 -rz];
r2=[r2x 0 rz];

nr1=norm(r1);
nr2=norm(r2);

dnr1=errs(6);
dnr2=1/nr2*( (par(8)*errs(8))^2+(par(7)*errs(7))^2 )^0.5;

phi=atan(r2(1)/r2(3));
dphi=1/(sqrt(1+(r2(1)/r2(3))^2)) * ((errs(8)/par(7))^2+((par(8)/par(7)^2)*errs(7))^2)^0.5;

theta=pi-phi;
dtheta=dphi;

disp('r1 dr1 r2 dr2 theta dtheta')
disp([num2str(nr1) ' ' num2str(dnr1) ' ' num2str(nr2) ' ' num2str(dnr2) ' ' num2str(180/pi*theta) ' ' num2str(180/pi*dtheta)]);
end

