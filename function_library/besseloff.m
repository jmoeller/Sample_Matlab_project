function [ J ] = besseloff( z, z0 )
% BESSELOFF attempts to calculate Bessel function with offset

J=0;
dtheta=0.01;

for theta=0:dtheta:pi
    J=J+1/pi*cos(z*sin(theta)+z0)*dtheta;
end

end

