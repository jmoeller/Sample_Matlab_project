function [ y ] = voigt_voigt(parall,x)
% Phenomenological fit of order parameter
% assumed sign of T is +

% BEGIN ESSENTIAL HEADER
% Generate variables from parall cell array
% Needs to be in every fitting function
for i=1:length(parall{1})
    varbls=genvarname(parall{1}{i});
    eval([varbls '=  parall{2}(i);']);
end
% END ESSENTIAL HEADER

%T=1.7;
%k=1.3806488e-23;
%ee=1.6021765e-19;

y=A*voigt(x,vc1,vg1,vl1)+B*voigt(x,vc2,vg2,vl2)+ yoff;
%y=1./(1.01-exp(x*1e-3*ee/k/T)).*(A*voigt(x,vc1,vg1,vl1)+B*voigt(x,vc2,vg2,vl2)+ yoff);
        
end
