function [ F ] = Keren1(parall,t)
% FmuF signal with Lorentzian damping, Lorentzian background and
% non-decaying background
% Simplified version: no B field and no t0 offset (less parameters in fit
% table).

% BEGIN ESSENTIAL HEADER
% Generate variables from parall cell array
% Needs to be in every fitting function
for i=1:length(parall{1})
    varbls=genvarname(parall{1}{i});
    eval([varbls '=  parall{2}(i);']);
end
% END ESSENTIAL HEADER


F=A*(r*exp(-(D^2*t.^2)/2)+(1-r)*(a*exp(-sqrt(lambda1*t))+(1-a)*exp(-sqrt(lambda2*t)).*cos(w*t)));

end
