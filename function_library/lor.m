function [ y ] = lor(parall,x)
% Just a Lorentzian + a background

% BEGIN ESSENTIAL HEADER
% Generate variables from parall cell array
% Needs to be in every fitting function
for i=1:length(parall{1})
    varbls=genvarname(parall{1}{i});
    eval([varbls '=  parall{2}(i);']);
end
% END ESSENTIAL HEADER

y=A1.*exp(-la1*x) + Abg;
end
