function [ y ] = fd(parall,x)
% Phenomenological fit of order parameter
% assumed sign of T is +

% BEGIN ESSENTIAL HEADER
% Generate variables from parall cell array
% Needs to be in every fitting function
for i=1:length(parall{1})
    varbls=genvarname(parall{1}{i});
    eval([varbls '=  parall{2}(i);']);
end
% END ESSENTIAL HEADER

y=zeros(length(x),1);

for i=1:length(x)
    if (T~=0)
        y(i)=y0/(exp((x(i)-chempot)/T)+1.0)+ybg;
    else
        if(x<chempot)
            y(i)=y0+ybg;
        else
            y(i)=ybg;
        end
    end
    
end
