function [ F ] = FmuF_LRO_3(parall,t)
% FmuF signal with Lorentzian damping, Lorentzian background and
% non-decaying background
% Allows for non-uniform B field
% B(1,i) is for Bi for F1, B(2,i) is for F2!, B(3,i) is for mu (careful)

% BEGIN ESSENTIAL HEADER
% Generate variables from parall cell array
% Needs to be in every fitting function
for i=1:length(parall{1})
    varbls=genvarname(parall{1}{i});
    eval([varbls '=  parall{2}(i);']);
end
% END ESSENTIAL HEADER

phi=pi/180*phi; % convert to radians!! 
r1=[0 0 -mr1];
r2=mr2*[sin(phi) 0 cos(phi)];
% r1=[0 0 -mr];
% r2=mr*[sin(phi) 0 cos(phi)];


rm=[0 0 0];
B=1e-4*[B1x B1y B1z; B2x B2y B2z; Bmx Bmy Bmz]; % non-uniform field see above
                                                % convert from G to T
%[Ptot] = fmuf(B,r1,rm,r2,t,A_muF,A_FF) las two optional
Gfmuf=fmuf(B,r1,rm,r2,t);

F=A1*Gfmuf.*exp(-la1*t)+A2*exp(-la2*t)+Abg+A3*(1/3+2/3*exp(-(sig*t).^2).*cos(2*pi*nu*t))+A4*(1/3+2/3*exp(-(sig2*t).^2).*cos(2*pi*nu2*t));

end
