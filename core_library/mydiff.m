function [ F ] = mydiff(parfree,myfitfun,parall,x,y,yerr)
% Calculates weighted difference between predicted (fitted) and observed
% Last Update: JSM, Aug 2011

[parall] = update_parall(parall,parfree);

F=y-myfun(myfitfun,parall,x);
F=F./(yerr); % weigh the difference with 1/err^2

end
