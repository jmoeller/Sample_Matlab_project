function [ ] = dispguess(myfitfun,parall,parfreenames,parfree,parfixed,chi2,redchi2,t,y,yerr,axs,printerrs)
% Displays the guess
% Last Update: JSM, Aug 2011

disp('------------------------------------------------------------------');

disp('Fixed parameters');
for i=1:length(parfixed{1})
    disp([parfixed{1}{i} ' = ' num2str(parfixed{2}(i) ) ]);
end

disp('------------------------------------------------------------------');

disp('Free parameters');
for i=1:length(parfreenames)
    disp([parfreenames{i} ' = ' num2str(parfree(i))]);
end

disp('------------------------------------------------------------------');

disp([ 'reduced chi2' ' = ' num2str(redchi2) ]);
disp([ 'chi2' ' = ' num2str(chi2) ]);
plotfitdat(myfitfun,t,y,yerr,parall,axs,printerrs);

end

