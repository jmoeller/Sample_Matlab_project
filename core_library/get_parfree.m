function [parfreenames parfree lb ub parfixed] = get_parfree(parall)
% Gets parfree (just the free parameters not their names) lb ub pafreenames
% the names of the free parameters and parfixed the names and values of 
% the fixed parameters from parall
% Last Update: JSM, Aug 2011

nfree=sum(parall{5}(:));
nfixed=length(parall{2})-nfree;

parfree=zeros(1,nfree);
lb=zeros(1,nfree);
ub=zeros(1,nfree);
parfreenames=cell(1,nfree);
parfixed=cell(1,2);
parfixed{1}=cell(1,nfixed);
parfixed{2}=zeros(1,nfixed);

pos=1;
pos2=1;
for i=1:length(parall{2})
    if (parall{5}(i)==1)
        parfreenames{pos}=parall{1}{i};
        parfree(pos)=parall{2}(i);
        lb(pos)=parall{3}(i);
        ub(pos)=parall{4}(i);
        pos=pos+1;
    else
        parfixed{1}{pos2}=parall{1}{i};
        parfixed{2}(pos2)=parall{2}(i);
        pos2=pos2+1;
    end
    
end


end

