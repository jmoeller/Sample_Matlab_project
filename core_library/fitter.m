%% Import data
datdir='/Users/jsm/dphil/projects/archive/PSI/2012-October/GPS/';
anadir='/Users/jsm/dphil/projects/archive/PSI/2012-October/GPS/';
infileonly='cof2_50k'; infileandpath=strcat(datdir,infileonly);
fittab='cof2_mat1'; saveall=1;
data=importdata(strcat(infileandpath,'.dat')); headerdat=importdata(strcat(infileandpath,'.info'));
x=data(:,1); y=data(:,2); yerr=data(:,3); Temp=headerdat(1); Field=headerdat(2); %Temp=0; Field=0;
axs=[0 5 5 30]; printerrs=0; plotinidat(x,y,yerr,axs,printerrs);
%% Initialise parameters
myfitfun='FmuF_LRO_s'; parall=cell(1,5);
parall{1}={'A1' 'la1' 'A2' 'la2' 'Abg' 'mr1' 'mr2' 'phi' 'A3' 'sig' 'nu' 'A4' 'sig2' 'nu2'}; % [] don't work
parall{2}=[12    0.3   4    0.03 0.0 1.2981 1.1459 0      0     0    0   0     0      0];     % value
%% Set free or fixed flags and prepare fit
parall{3}=[0    0.0    0    0.0    0     0     0     0    0      0     0    0     0    0];      % lb
parall{4}=[Inf  Inf   Inf   Inf   Inf    Inf   Inf  180   Inf  Inf  Inf  Inf   Inf    Inf];    % ub
parall{5}=[1     1     1    1     1      1     1    0    0     0    0  0     0     0    ];     % 1=free,0=fxd[parfreenames parfree lb ub parfixed] = get_parfree(parall);
[parfreenames parfree lb ub parfixed] = get_parfree(parall);
options = optimset('MaxFunEvals',4000); %,'TolFun',1e-11,'FinDiffType','central','GradObj','on'); %'Display','iter',
[chi2 redchi2] = get_chi2(myfitfun,x,y,yerr,parall);
dispguess(myfitfun,parall,parfreenames,parfree,parfixed,chi2,redchi2,x,y,yerr,axs,printerrs);
%% Fit
[parfree,resnorm,residual,exitflag,fitinfo,lambda,jacobian] = lsqnonlin(@(parfree) mydiff(parfree,myfitfun,parall,x,y,yerr),parfree,lb,ub,options);
parall= update_parall(parall,parfree); % in case of reverting after last call
ci=nlparci(parfree,residual,'jacobian',jacobian);%,'alpha',0.3173); % returns 68.27% ci
[chi2 redchi2] = get_chi2(myfitfun,x,y,yerr,parall);
[errsfree errsall]=get_errs(ci,parall);
procres(myfitfun,Temp,Field,x,y,yerr,axs,printerrs,parall,parfreenames,parfree,errsfree,parfixed,lb,ub,fitinfo,exitflag,ci,chi2,redchi2,anadir,infileonly,infileandpath,fittab);