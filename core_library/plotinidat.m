function [  ] = plotinidat(t,y,yerr,axs,printerrs)
% Plots data and fits
% Last Update: JSM, Aug 2011

clf;
if (printerrs==1)
    errorbar(t,y,yerr,'+k'); % black +
else 
    plot(t,y,'-k');
end
xlabel('x');
ylabel('y');
axis(axs);

end

