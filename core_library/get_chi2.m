function [ chi2 redchi2] = get_chi2(myfitfun,x,y,yerr,parall)
% Calculates chi2 and reduced chi2 
% Last Update: JSM, Aug 2011

nfree=sum(parall{5}(:));
chi2=0;
expected=myfun(myfitfun,parall,x);

for i=1:length(x)
    chi2=chi2+( (y(i)-expected(i))/yerr(i) )^2;
end

redchi2=chi2/(length(x)-nfree);

end

