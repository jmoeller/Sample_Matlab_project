% savefit.m
% Saves fit and data to data file named after orginal file except with
% ending .fit
% Last Update: JSM, Aug 2011

%% Generate output filename
% BEGIN ESSENTIAL HEADER
% Generate variables from parall cell array
% Needs to be in every fitting function
for i=1:length(parall{1})
    varbls=genvarname(parall{1}{i});
    eval([varbls '=  parall{2}(i);']);
end
% END ESSENTIAL HEADER

%fitout=strcat(anadir,infileonly,'-Bxm',num2str(Bxm),'.fit');
fitout=strcat(anadir,infileonly,'-',fittab,'.fit');

%% Save output data including parameter values and header
tabout=get_tabout(infileonly,Temp,Field,redchi2,parall,parfreenames,parfree,errsall,errsfree,0);
fitoutptr=fopen(fitout, 'w');

% Write header
fprintf(fitoutptr,'! %s \n', datestr(now)); % %%=literal % for GLE comment
fprintf(fitoutptr, '! Fit model: %s \n', myfitfun); % fit model
fprintf(fitoutptr,'! Fixed parameters: ');
for i=1:length(parall{1})
    if(parall{5}(i)==0) % if parameter was fixed
        fprintf(fitoutptr,'%s = %8.6f ',parall{1}{i}, parall{2}(i));
    end
end
fprintf(fitoutptr,'\n'); 
fprintf(fitoutptr,'! Free parameters: ');
for i=1:length(parall{1})
    if(parall{5}(i)==1) % if parameter was fixed
        fprintf(fitoutptr,'%s = %8.6f ',parall{1}{i}, parall{2}(i));
    end
end

fprintf(fitoutptr,'\n! ');

for i=1:length(tabout{1})
    fprintf(fitoutptr,'(%d) ',i); % prints numbering of columns
end
fprintf(fitoutptr,'\n! '); 
for i=1:length(tabout{1})
    fprintf(fitoutptr,'%s ',tabout{1}{i}); % prints column labels
end
fprintf(fitoutptr, '\n');

% Print run number (string)
fprintf(fitoutptr, '! %s ',tabout{2}{1});
for i=2:length(tabout{2})
    % Print variable values
    fprintf(fitoutptr, '%8.6f ',tabout{2}{i});
end
fprintf(fitoutptr, '\n');

%
% Specify starting and end values for x for output of fit
xstart=0.5*min(x);%min(x); %min(x);
xend=2*max(x); 
padding=500; % Minimum length of x vector, padding if length is less

% Check whether x is long enough otherwise pad it linearly
if (length(x)<padding)
    disp('here')
    xnew=(linspace(xstart,xend,padding))';
else
    xnew=x;
end

expected=myfun(myfitfun,parall,xnew);

fprintf(fitoutptr,'! \n');
fprintf(fitoutptr,'! x yfit \n');
for i=1:length(xnew)
    fprintf(fitoutptr,'%8.6f  %8.6f \n',xnew(i),expected(i));
end

fclose(fitoutptr);

disp(['Fit stored in:' fitout]);
%[infileandpathwoexten, ~]=textscan(infile, '%s %s', 'delimiter', '.');
% Careful this returns a cell array, not a string.
% Can use this code below or just use infileandpathwoexten{1}
% if iscell(infileandpathwoexten)
%     infileandpathwoexten=infileandpathwoexten{1};
% end