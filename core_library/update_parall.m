function [ parall ] = update_parall(parall,parfree)
% Updates parall from most recent parfree
% Last Update: JSM, Aug 2011

pos=1;
for i=1:length(parall{2})
    if(parall{5}(i)==1) % only if component is free it could have changed
        parall{2}(i)=parfree(pos);
        pos=pos+1;
    end
end

end

