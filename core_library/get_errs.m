function [errsfree errsall]=get_errs(ci,parall)
% Calculates 1 sigma standard errors from 68% confidence interval
% Last Update: JSM, Aug 2011

errsfree=zeros(1,size(ci,1));
for i=1:size(ci,1)
    errsfree(i)=0.5*abs(ci(i,1)-ci(i,2));
end

errsall=zeros(1,length(parall{2}));

pos=1;
for i=1:length(parall{2})
    if(parall{5}(i)==1)                 % only if component is free 
        errsall(i)=errsfree(pos);       % we have an error estimate
        pos=pos+1;
    else 
        errsall(i)=0;                   % otherwise set error=0
    end
    

end

end

