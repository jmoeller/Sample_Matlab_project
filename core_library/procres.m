function [ ] = procres(myfitfun,Temp,Field,t,y,yerr,axs,printerrs,parall,parfreenames,parfree,errsfree,parfixed,lb,ub,fitinfo,exitflag,ci,chi2,redchi2,anadir,infileonly,infileandpath,fittab)
% Processes the resuls. 
% Displays the results, writes them to fitlog file filename.fitlog and
% writes parall to Matlog .mat file for convenient loading

% Plot results 
plotfitdat(myfitfun,t,y,yerr,parall,axs,printerrs);

% Generate fitlog output file name
fitlog=strcat(anadir,infileonly,'.log');
% Generate Matlab .mat file for variables
matlog=strcat(anadir,infileonly,'-',fittab,'.mat');

% Save all parameters to matlog file .mat
save(matlog);

% Start logged output of fitlog
diary(fitlog) 

disp(datestr(now));

disp('Run (.dat and .info files)');
disp(infileandpath);
disp(' ');

disp(['Temperature (K): ' num2str(Temp)]);
disp(['Field (G): ' num2str(Field)]);

disp(['Fit function: ' myfitfun]);

disp(' ');

disp('exitflag');
disp(exitflag);

disp('Lower and upper bounds for fit (constraints)');
disp(lb);
disp(ub);

disp('------------------------------------------------------------------');
disp('------------------------------------------------------------------');

disp('Fixed parameters');
for i=1:length(parfixed{1})
    disp([parfixed{1}{i} ' = ' num2str(parfixed{2}(i) ) ]);
end

disp('------------------------------------------------------------------');

disp('Free parameters');
for i=1:length(parfreenames)
    disp([parfreenames{i} ' = ' num2str(parfree(i)) ' +/- ' num2str(errsfree(i)) ]);
end

disp('------------------------------------------------------------------');

disp([ 'reduced chi2' ' = ' num2str(redchi2) ]);
disp([ 'chi2' ' = ' num2str(chi2) ]);

disp('------------------------------------------------------------------');
disp('------------------------------------------------------------------');

disp('ci');
disp(ci);


disp('========================================================================');

diary off

end

% To cut a filename into pieces by specifying extensions use
%[infileonlywoexten, ~]=textscan(infileonly, '%s %s', 'delimiter', '.');
% Careful this returns a cell array, not a string. 
% Can use code in footnote or just infileonlywoexten{1}
% if iscell(infileandpathwoexten)
%     infileandpathwoexten=infileandpathwoexten{1};
% end



