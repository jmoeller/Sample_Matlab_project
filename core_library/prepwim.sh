#!/bin/sh
# Removes header lines starting with ! from .wim files and stores the bulk in file.dat
# Then greps Temperature and Field from .wim files and stores successively in .info file

#cd "/Users/Shared/DPhil/Samples/AgF2/analysis/Aug2011/b1"
for file in `ls *.wim` ; do						# for each file ending on .wim 
echo $file '>' "`basename "$file" .wim`.dat" # indicate what will happen (just output)
grep -v ! $file > "`basename "$file" .wim`.dat" # output all lines NOT starting with ! and pipe output to 
															# file named the same way but with different ending															
grep Temperature $file | cut -f2 -d: | cut -f1 -dK	>	"`basename "$file" .wim`.info"
grep Field $file | cut -f2 -d: | cut -f1 -dG	>>	"`basename "$file" .wim`.info"													
done

