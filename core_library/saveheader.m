% saveheader.m
% Save header to fit tab 
tabout=get_tabout(infileonly,Temp,Field,redchi2,parall,parfreenames,parfree,errsall,errsfree,saveall);

fittabfile=strcat(anadir,fittab,'.dat'); 
fittabptr=fopen(fittabfile, 'a'); % append!

fprintf(fittabptr,'! %s \n', datestr(now)); % !=literal % for GLE comment
fprintf(fittabptr, '! Fit model: %s \n', myfitfun); % fit model
fprintf(fittabptr,'! Fixed parameters: ');
for i=1:length(parall{1})
    if(parall{5}(i)==0) % if parameter was fixed
        fprintf(fittabptr,'%s = %8.6f ',parall{1}{i}, parall{2}(i));
    end
end
fprintf(fittabptr,'\n! '); 
for i=1:length(tabout{1})
    fprintf(fittabptr,'(%d) ',i); % prints numbering of columns
end
fprintf(fittabptr,'\n! '); 
for i=1:length(tabout{1})
    fprintf(fittabptr,'%s ',tabout{1}{i}); % prints column labels
end
fprintf(fittabptr, '\n');

fclose(fittabptr);

disp('Header stored in');
disp(fittabfile);
