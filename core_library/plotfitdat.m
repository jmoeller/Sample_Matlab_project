function [  ] = plotfitdat(myfitfun,x,y,yerr,parall,axs,printerrs)
% Plots data and fits
% Last Update: JSM, Aug 2011

% Check whether x is long enough otherwise pad it linearly
if (length(x)<20)
    xnew=linspace(x(1),x(end),20)';
else
    xnew=x;
end

clf;
if (printerrs==1)
    errorbar(x,y,yerr,'+k');
else 
    plot(x,y,'-k');
end
hold on
plot(xnew,myfun(myfitfun,parall,xnew),'r',x,1*(y-myfun(myfitfun,parall,x)),'b+');
xlabel('x');
ylabel('y');
axis(axs);
%h=legend('data','fit','residual');

end

