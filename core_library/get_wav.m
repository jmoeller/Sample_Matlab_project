% Calculates weighted or unweighted averages of columns of data
%
% INPUT: get_wav('file.dat',data column, error column (optional)). 
% If error column is not specified, calculates the unweighted
% average and its standard deviation.
%
% OUTPUT: weighted average, std dev, estimated error

% See note on importing data below
function [wav wavstd waverr] = get_wav(myfile,cy,cerr)

if nargin < 3, weighted=0; else weighted=1; end

% Import data
mydat=importdata(myfile);

y=mydat(:,cy);
wavstd=std(y);

disp(['First data point: ' num2str(y(1))]);

if (weighted==0)  % unweighted average
    wav=mean(y);
    waverr=0;
else
    yerr=mydat(:,cerr);
    weights=1./(yerr.^2);       % weights(i)=1/yerr(i)^2
    wav=weights'*y/sum(weights);
    waverr=1/sqrt(sum(weights));
end
end

% Importing data
% dlmread: gets delimiters write and allows header lines to be specified
% but does not understand comments, hence when we comment out data in the
% middle of the file it gets confused.
% textscan: understands comments but needs to know the number of columns
% before writing the .m file even (at least to be convenient, could
% convert the linear array it outputs into a matrix). 
% importdata: understands Matlab comments (%) and even comments in a
% header, but if there are comments other than starting with % in the main
% body it gets confused. 
% Solution: use % as comment in fit tables and tell GLE to use % as
% comment. Then importdata works fine. 

%throwawaydat=dlmread(myfile, '', nhead, 0); % '' is for the delimiter, see doc why ''
% fid = fopen(myfile);
% row = 1;
% while (~feof(fid)) 
%     if (ncol==1)
%         mydat(row,1) = textscan(fid,'%f','CommentStyle','!'); % a double, done
%     else 
%         mydat(row,1) = textscan(fid,'%s','CommentStyle','!'); % a string, continue
%     for i=2:ncol
%         mydat(row,i) = textscan(fid,'%f','CommentStyle','!');
%     end
%     textscan(fid,'\n','CommentStyle','!');
%     row = row + 1;
% end
% fclose(fid);
