%% Import data
datdir='/Users/Shared/DPhil/Samples/AgF2/analysis/Aug2011/b1/';
anadir='/Users/Shared/DPhil/Samples/AgF2/analysis/Aug2011/b1/';
infileonly='nu1tab6b'; infileandpath=strcat(datdir,infileonly);
fittab='none'; fittabfile=strcat(anadir,fittab,'.dat'); saveall=1;
data=importdata(strcat(infileandpath,'.dat')); %headerdat=importdata(strcat(infileandpath,'.info'));
x=data(:,2); y=data(:,3); yerr=data(:,4); Temp=0; Field=0; % need to be 0 whatever they really are, use T insetad
axs=[140 170 0 12]; printerrs=1; plotinidat(x,y,yerr,axs,printerrs);
%% Initialise parameters
myfitfun='op'; parall=cell(1,6);
parall{1}={'nu0' 'nubg' 'Tc' 'alp' 'be'}; % [] don't work
parall{2}=[20      0     162     1     0.5 ];     % value
%% Set free or fixed flags and prepare fit
parall{3}=[0     0.0     0.0     0      0];          % lb
parall{4}=[Inf   Inf     Inf   Inf   Inf];            % ub
parall{5}=[1     0       0      0       1];     % 1=free,0=fxd
[parfreenames parfree lb ub parfixed] = get_parfree(parall);
options = optimset('MaxFunEvals',4000); %,'TolFun',1e-11,'FinDiffType','central','GradObj','on'); %'Display','iter',
[chi2 redchi2] = get_chi2(myfitfun,x,y,yerr,parall);
dispguess(myfitfun,parall,parfreenames,parfree,parfixed,chi2,redchi2,x,y,yerr,axs,printerrs);
%% Fit
[parfree,resnorm,residual,exitflag,fitinfo,lambda,jacobian] = lsqnonlin(@(parfree) mydiff(parfree,myfitfun,parall,x,y,yerr),parfree,lb,ub,options);
parall= update_parall(parall,parfree); % in case of reverting after last call
ci=nlparci(parfree,residual,'jacobian',jacobian);%,'alpha',0.3173); % returns 68.27% ci
[chi2 redchi2] = get_chi2(myfitfun,x,y,yerr,parall);
[errsfree errsall]=get_errs(ci,parall);
procres(myfitfun,Temp,Field,x,y,yerr,axs,printerrs,parall,parfreenames,parfree,errsfree,parfixed,lb,ub,fitinfo,exitflag,ci,chi2,redchi2,anadir,infileonly,infileandpath,fittab);