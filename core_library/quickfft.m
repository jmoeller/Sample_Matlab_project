function [ f, powr ] = quickfft(x,y,windowtype,pad,axs)
%quickfft

% f is linear (not angular) frequency in the same units as 1/x, 
% powr the amplitude, NB: when using a window (other than a rectangle) the
% absolute value of this is meaningless
% padding makes the peak sharper and the value of the maximum pwr more
% correct for rectangular windowing (gives the Fourier coefficient) but
% introduces ringing around the peak

dx=x(2)-x(1);
Fs=1/dx;
L=length(x);

% Create window
switch windowtype
    case 1 % Rectangle = no windowing
        window=rectwin(L);
    case 2 % Use Hamming window
        window=hamming(L);
    case 3 % Hann
        window=hann(L);
    case 4 % Triangle
        window=triang(L);
    case 5 % Barlett
        window=barlett(L);
    case 6 % Blackmann
        window=blackmann(L);
    case 7 % Chebyshev
        window=chebwin(L);
    case 8 % Taylor
        window=taylorwin(L);
end

% Perform windowing - ie mask signal with window
y=y.*window;

nfft=pad*2^nextpow2(L); % fft works best if length of interval
% is a power of 2, pad extra bit with 0s
% pad is the padding factor to improve
% accuracy
yfft=fft(y,nfft);       % Perform Fourier transform

%Calculate the number of unique points N
N=ceil((1+nfft)/2);    % Valid both for odd and even nfft

% yfft is symmetric so through away second half
yfft=yfft(1:N);

% Normalise power so it does not depend on length of data set
% L is the length of the data, not nfft as we pad with 0s
% factor of 4 is to ensure correct normalisation
% f=0 and Nyquist component are unique so must not be multiplied by 4
powr=(abs(yfft)/L).^2;
powr(2:end -1) = powr(2:end -1)*4;

% Now create frequency vector
f = (0:N-1)*Fs/nfft;

plot(f,powr);
axis(axs);

% Find index fmax of maximum power, this is more efficient than
% find(pow=max(pow)), returns an logical index vector
%fmax = powr==max(powr);

% Determine value of power at fmax for diagnostic purposes
%powF1=powr(fmax);

% Calculate frequency at this index
%F1=f(fmax);

% Calculate FWHM of the power spectrum at the dominant peak
%dF1=0.5*fwhm(f,powr);

end

