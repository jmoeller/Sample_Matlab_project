function [ F ] = myfun(myfitfun,parall,x)
% This file calls the fitting function sepcified by myfitfun
% Last Update: JSM, Aug 2011

 eval(['F=' myfitfun '(parall,x);']); 
end