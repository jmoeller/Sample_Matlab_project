% savefittab.m
% Save fittable to file 

% Get latest tabout cell array (tabout{2} may have changed)
tabout=get_tabout(infileonly,Temp,Field,redchi2,parall,parfreenames,parfree,errsall,errsfree,saveall);

fittabfile=strcat(anadir,fittab,'.dat'); 
fittabptr=fopen(fittabfile, 'a'); % append!

% Print run number (string)
fprintf(fittabptr, '%s ',tabout{2}{1});
for i=2:length(tabout{2})
    % Print variable values
    fprintf(fittabptr, '%8.6f ',tabout{2}{i});
end
fprintf(fittabptr, '\n');

fclose(fittabptr);

disp('Output stored in');
disp(fittabfile);

