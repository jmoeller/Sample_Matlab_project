function tabout = get_tabout(infileonly,Temp,Field,redchi2,parall,parfreenames,parfree,errsall,errsfree,saveall)
% Calulates and returns the tabout cell array for writing to the fit table
% tabout contains variable names (parameters and errors) and their values
% if saveall==1 then it contains all parameters, if saveall~=1 it contains
% just the free parameters

tabout=cell(1,2);

if(saveall==1)
    tabout{1}=cell(1,2*length(parall{2})+4);    
    tabout{2}=cell(1,2*length(parall{2})+4);    
    for i=1:length(parall{2})
        tabout{1}{2*i+3}=parall{1}{i};
        tabout{1}{2*i+4}='(Err)';
        tabout{2}{2*i+3}=parall{2}(i);
        tabout{2}{2*i+4}=errsall(i);
    end
else
    tabout{1}=cell(1,2*length(parfree)+4); 
    tabout{2}=cell(1,2*length(parfree)+4);
    for i=1:length(parfree)
        tabout{1}{2*i+3}=parfreenames{i};
        tabout{1}{2*i+4}='(Err)';
        tabout{2}{2*i+3}=parfree(i);
        tabout{2}{2*i+4}=errsfree(i);
    end
end

tabout{1}{1}='Run'; 
tabout{1}{2}='RC2';
tabout{1}{3}='T(K)';
tabout{1}{4}='B(G)';
tabout{2}{1}=infileonly; 
tabout{2}{2}=redchi2; 
tabout{2}{3}=Temp; 
tabout{2}{4}=Field;

end

