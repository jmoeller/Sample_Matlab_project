Motivation:
This was a small side project of mine to develop a versatile 1d fitting program in Matlab. 
Existing programs are either incorporated in large commerical software (such as Origin by Orignlabs)
or are part of scientific packages for a specific purpose (such as analysis neutron powder diffraction
data). I wanted a light-weight, easy to modify and versatile library. I chose Matlab as it has a lot
of basic infrastructre (such as Levenberg Marquadt routines) already built in. 

Contents:
User interface: via a script (to be versatile), examples can be found in sample_fitting_script
Fitting infrastructure: located in core_library, should not be modified by user, needs to be in Matlab path
Function library: contained in function_library. Can be extended to include any 1d function (also non-analytic ones)

