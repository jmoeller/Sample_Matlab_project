%% Import data
datdir='/Users/jsm/DPhil/projects/phcc/SNS_April-14/offline_jan15/';
anadir='/Users/jsm/DPhil/projects/phcc/SNS_April-14/offline_jan15/';
infileonly='0kbar_h_0p99'; infileandpath=strcat(datdir,infileonly);
fittab='gauss'; saveall=1;
data=importdata(strcat(infileandpath,'.dat')); %headerdat=importdata(strcat(infileandpath,'.info'));
x=data(89:end,1); y=data(89:end,2); yerr=ones(size(y)); Temp=0; Field=0; %Temp=headerdat(1); Field=headerdat(2); 
axs=[0 3.2 0 0.01]; printerrs=0; plotinidat(x,y,yerr,axs,printerrs);
%% Initialise parameters
myfitfun='gauss'; parall=cell(1,5);
parall{1}={'A'    'yoff' 'xc'   'sig' }; % [] don't work
parall{2}=[0.005   0.001  2.5   0.1 ];     % value
%% Set free or fixed flags and prepare fit
parall{3}=[-Inf   -Inf   0      0 ];             % lb
parall{4}=[Inf   Inf     Inf   Inf ];            % ub
parall{5}=[1     1       1      1];
[parfreenames parfree lb ub parfixed] = get_parfree(parall);
options = optimset('MaxFunEvals',4000); %,'TolFun',1e-11,'FinDiffType','central','GradObj','on'); %'Display','iter',
[chi2 redchi2] = get_chi2(myfitfun,x,y,yerr,parall);
dispguess(myfitfun,parall,parfreenames,parfree,parfixed,chi2,redchi2,x,y,yerr,axs,printerrs);
%% Fit
[parfree,resnorm,residual,exitflag,fitinfo,lambda,jacobian] = lsqnonlin(@(parfree) mydiff(parfree,myfitfun,parall,x,y,yerr),parfree,lb,ub,options);
parall= update_parall(parall,parfree); % in case of reverting after last call
ci=nlparci(parfree,residual,'jacobian',jacobian,'alpha',0.3173); % returns 68.27% ci
[chi2 redchi2] = get_chi2(myfitfun,x,y,yerr,parall);
[errsfree errsall]=get_errs(ci,parall);
procres(myfitfun,Temp,Field,x,y,yerr,axs,printerrs,parall,parfreenames,parfree,errsfree,parfixed,lb,ub,fitinfo,exitflag,ci,chi2,redchi2,anadir,infileonly,infileandpath,fittab);


 