%% Import data
datdir='/Users/jsm/dphil/projects/cr8mn/data/';
anadir='/Users/jsm/dphil/projects/cr8mn/offline_jan14/';
infileonly='39533'; infileandpath=strcat(datdir,infileonly);
fittab='amrule1'; saveall=1;
data=importdata(strcat(infileandpath,'.dat')); headerdat=importdata(strcat(infileandpath,'.info'));
x=data(:,1); y=data(:,2); yerr=data(:,3); Temp=headerdat(1); Field=headerdat(2); %Temp=0; Field=0;
axs=[0 3 19 24]; printerrs=0; plotinidat(x,y,yerr,axs,printerrs);
%% Initialise parameters
myfitfun='lor'; parall=cell(1,5);
parall{1}={'A1'  'la1'  'Abg' }; % [] don't work
parall{2}=[  3    5      1.5  ];     % value
%% Set free or fixed flags and prepare fit
parall{3}=[  0    0.0     0    ];   % lb
parall{4}=[5      5      5     ];   % ub
parall{5}=[1      1      0     ];   % 1=free,0=fxd[parfreenames parfree lb ub parfixed] = get_parfree(parall);
[parfreenames parfree lb ub parfixed] = get_parfree(parall);
options = optimset('MaxFunEvals',4000); %,'TolFun',1e-11,'FinDiffType','central','GradObj','on'); %'Display','iter',
[chi2 redchi2] = get_chi2(myfitfun,x,y,yerr,parall);
dispguess(myfitfun,parall,parfreenames,parfree,parfixed,chi2,redchi2,x,y,yerr,axs,printerrs);
%% Fit
[parfree,resnorm,residual,exitflag,fitinfo,lambda,jacobian] = lsqnonlin(@(parfree) mydiff(parfree,myfitfun,parall,x,y,yerr),parfree,lb,ub,options);
parall= update_parall(parall,parfree); % in case of reverting after last call
ci=nlparci(parfree,residual,'jacobian',jacobian);%,'alpha',0.3173); % returns 68.27% ci
[chi2 redchi2] = get_chi2(myfitfun,x,y,yerr,parall);
[errsfree errsall]=get_errs(ci,parall);
procres(myfitfun,Temp,Field,x,y,yerr,axs,printerrs,parall,parfreenames,parfree,errsfree,parfixed,lb,ub,fitinfo,exitflag,ci,chi2,redchi2,anadir,infileonly,infileandpath,fittab);